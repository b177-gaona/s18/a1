/*
1. In the s18 folder, create an a1 folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.
3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
- Name (String)
- Age (Number)
- Pokemon (Array)
- Friends (Object with Array values for properties)
5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
6. Access the trainer object properties using dot and square bracket notation.
7. Invoke/call the trainer talk object method.
8. Create a constructor for creating a pokemon with the following properties:
- Name (Provided as an argument to the contructor)
- Level (Provided as an argument to the contructor)
- Health (Create an equation that uses the level property)
- Attack (Create an equation that uses the level property)
9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
11. Create a faint method that will print out a message of targetPokemon has fainted.
12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
13. Invoke the tackle method of one pokemon object to see if it works as intended.
14. Create a gitlab repository named a1 in S18.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle.
*/

/*

3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
- Name (String)
- Age (Number)
- Pokemon (Array)
- Friends (Object with Array values for properties)
5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
*/
function Trainer(name, age, pokemon, friends){
	// Properties
	this.name = name;
	this.age = age;
	this.pokemon = pokemon;
	this.friends = friends;
	// Methods
	this.talk = function(selectedPokemon){
		console.log(selectedPokemon + "! I choose you!");
	}
}

let ash = new Trainer(
		"Ash Ketchum", 
		10, 
		['Pikachu','Charizard','Squirtle','Bulbasaur'], 
		{
			hoenn: ['May','Max'],
			kanto: ['Brock','Misty'],
		}
	)
console.log(ash);

// 6. Access the trainer object properties using dot and square bracket notation.
console.log("Result from dot notation:"); 
console.log(ash.name);

console.log("Result of square bracket notation:");
console.log(ash['pokemon']);

// 7. Invoke/call the trainer talk object method.
console.log("Result of talk method:");
console.log(ash.talk('Pikachu'));

/*
8. Create a constructor for creating a pokemon with the following properties:
- Name (Provided as an argument to the contructor)
- Level (Provided as an argument to the contructor)
- Health (Create an equation that uses the level property)
- Attack (Create an equation that uses the level property)
10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
11. Create a faint method that will print out a message of targetPokemon has fainted.
12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
*/

// Object Constructor
function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	// Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		newhealth = (target.health - this.attack);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		if (newhealth <= 0) {
			target.faint();
		}
	}
	this.faint = function(){
		console.log(this.name + ' fainted');
	}
}

// 9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
// 		['Pikachu','Charizard','Squirtle','Bulbasaur'], 
let pikachu = new Pokemon("Pikachu", 22);
let charizard = new Pokemon("Charizard", 36);
let squirtle = new Pokemon("Squirtle", 16);
let bulbasaur = new Pokemon("Bulbasaur", 16);
let charmander = new Pokemon("Charmander", 16);
let caterpie = new Pokemon("Caterpie", 7);

console.log(pikachu);
console.log(caterpie);
console.log(charmander);

pikachu.tackle(caterpie);
pikachu.tackle(charizard);